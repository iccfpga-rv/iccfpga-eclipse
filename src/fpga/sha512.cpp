/*
 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
 */

#include <string.h>

#include "sha512/sha512.h"
#include "fpga/sha512.h"
#include "fpga/fpga.h"

#include "micro-os-plus/diag/trace.h"



int SHA512FPGA::compress(sha512_context *md, unsigned char *buf) {
	(void) md;
// FPGA does little -> big endian
	memcpy((void*) FPGA::pidiverGetDataWritePtr(), buf, 128);

	FPGA::pidiverWrite (CMD_WRITE_FLAGS, FLAG_SHA512_START);
	while (FPGA::pidiverRead(CMD_READ_FLAGS) & FLAG_SHA512_RUNNING) ;
	return 0;
}

void SHA512FPGA::storeState(sha512_context *md, unsigned char *out) {
	(void) md;
// FPGA does big -> little endian
	memcpy(out, (void*) FPGA::getSHA512ReadPtr(), 64);
}

int SHA512FPGA::init(sha512_context *md) {
	if (md == NULL)
		return 1;

	md->curlen = 0;
	md->length = 0;

	FPGA::pidiverWrite (CMD_WRITE_FLAGS, FLAG_SHA512_RESET);
	return 0;
}



