/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
-- 
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/

#include <fpga/fpga.h>
#include "micro-os-plus/diag/trace.h"
#include "xparameters.h"
#include "gpio/gpio.h"
#include "iota/conversion.h"
#include <string.h>

//#include "sha512.h"

namespace FPGA {
	namespace {
		volatile uint32_t* pidiverPtr = ((uint32_t*) XPAR_AXI2LB_V1_0_0_BASEADDR);
		volatile uint32_t* dataWritePtr = (pidiverPtr + 0x80);
		volatile uint32_t* dataKeccak384ReadPtr = (pidiverPtr + 0x80 - 0x80 + 0x100);
		volatile uint32_t* dataTroikaReadPtr = (pidiverPtr + 0xa0 - 0x80 + 0x100);
		volatile uint32_t* dataCurlReadPtr = (pidiverPtr + 0xe0 /*0xc0*/ - 0x80 + 0x100);
		volatile uint32_t* dataSHA512ReadPtr = (pidiverPtr + 0x100 + 0x80);
		/*
		volatile uint32_t* fpga_base = (uint32_t*) XPAR_AXI2LB_V1_0_1_BASEADDR;
		volatile uint32_t* fpga_trits = (uint32_t*) (XPAR_AXI2LB_V1_0_1_BASEADDR + (0x200 + 0x20) * 4);
		volatile uint32_t* fpga_bigint = (uint32_t*) (XPAR_AXI2LB_V1_0_1_BASEADDR + (0x200 + 0x00) * 4);
*/
	}

//#include "Timer.h"

	volatile uint32_t* getSHA512ReadPtr() {
		return dataSHA512ReadPtr;
	}

	volatile uint32_t* pidiverGetDataWritePtr() {
		return dataWritePtr;
	}

	volatile uint32_t* troikaReadDataPtr() {
		return dataTroikaReadPtr;
	}

	volatile uint32_t* curlReadDataPtr() {
		return dataCurlReadPtr;
	}

	volatile uint32_t* keccak384ReadDataPtr() {
		return dataKeccak384ReadPtr;
	}
/*
	volatile uint32_t* converterGetTritsPtr() {
		return fpga_trits;
	}

	volatile uint32_t* converterGetBigintPtr() {
		return fpga_bigint;
	}
*/
	void pidiverWrite(uint32_t addr, uint32_t data) {
		*(pidiverPtr + addr) = data;
	}

	uint32_t pidiverRead(uint32_t addr) {
		return *(pidiverPtr + addr);
	}
/*
	void converterWrite(uint32_t addr, uint32_t data) {
		*(fpga_base + addr) = data;
	}

	uint32_t converterRead(uint32_t addr) {
		return *(fpga_base + addr);
	}
*/



}





