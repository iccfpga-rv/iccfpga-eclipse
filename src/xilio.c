
#include "xuartlite.h"
#include "xuartlite_i.h"
#include "xuartlite_l.h"

int read(int fd, char *buf, int nbytes) {
	int i;
	int numbytes = 0;
	char *LocalBuf = buf;

	if (fd < 0 || fd >= XPAR_XUARTLITE_NUM_INSTANCES)
		return -1;

	if (LocalBuf != NULL) {
		for (i = 0; i < nbytes; i++) {
			numbytes++;
			*(LocalBuf + i) = XUartLite_RecvByte(
					XUartLite_ConfigTable[fd].RegBaseAddr);
			if ((*(LocalBuf + i) == '\n') || (*(LocalBuf + i) == '\r')) {
				break;
			}
		}
	}
	return numbytes;
}

int write(int fd, char* buf, int nbytes)
{
	int i;
	char* LocalBuf = buf;

	if (fd < 0 || fd >= XPAR_XUARTLITE_NUM_INSTANCES)
		return -1;

	for (i = 0; i < nbytes; i++) {
		if (LocalBuf != NULL) {
			LocalBuf += i;
		}
		if (LocalBuf != NULL) {
			if (*LocalBuf == '\n') {
				XUartLite_SendByte(XUartLite_ConfigTable[fd].RegBaseAddr, '\r');
			}
			XUartLite_SendByte(XUartLite_ConfigTable[fd].RegBaseAddr, *LocalBuf);
		}
		if (LocalBuf != NULL) {
			LocalBuf -= i;
		}
	}
	return (nbytes);
}
