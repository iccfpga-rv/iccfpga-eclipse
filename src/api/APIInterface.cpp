/*
 * APIInterface.cpp
 *
 *  Created on: 28.06.2020
 *      Author: thomas
 */

#include <stdint.h>
#include <unistd.h>

#include "xparameters.h"
#include "xgpio.h"
#include "xdebug.h"
#include "xspi.h"		/* SPI device driver */
#include "xil_printf.h"

#include "gpio/gpio.h"

#include "api/APIInterface.h"


#define SPI_DEVICE_ID			XPAR_SPI_0_DEVICE_ID
#define SPI_SELECT 				0x01

static XSpi Spi;


APIUART::APIUART() {

}
APIUART::~APIUART() {

}

bool APIUART::init() {
	return true;
}

uint32_t APIUART::readln(void* buffer, uint32_t bufferSize) {
	return read(XPAR_AXI_UARTLITE_0_DEVICE_ID, (void*) buffer, bufferSize);
}

void APIUART::writeln(void* buffer, uint32_t bufferSize) {
	write(XPAR_AXI_UARTLITE_0_DEVICE_ID, buffer, bufferSize);
	write(XPAR_AXI_UARTLITE_0_DEVICE_ID, "\n", 1);
}


APISPI::APISPI() {}
APISPI::~APISPI() {}

bool APISPI::init() {

	int Status;
	XSpi_Config *ConfigPtr; /* Pointer to Configuration data */

	ConfigPtr = XSpi_LookupConfig(SPI_DEVICE_ID);
	if (ConfigPtr == NULL) {
		return false;
	}

	XSpi_Config slave;
	memcpy(&slave, ConfigPtr, sizeof(XSpi_Config));
	slave.SlaveOnly = 1;	// use master-config but switch to slave

	Status = XSpi_CfgInitialize(&Spi, ConfigPtr, ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		return false;
	}

	XSpi_Start(&Spi);
	XSpi_IntrGlobalDisable(&Spi);	// use polling-mode

	return true;
}



XSpi* APISPI::getSPI() {
	return &Spi;
}

void APISPI::flushReceiveBuffer() {
	while (!(XSpi_GetStatusReg(&Spi) & XSP_SR_RX_EMPTY_MASK)) {
		(void) XSpi_ReadReg(Spi.BaseAddr, XSP_DRR_OFFSET);
	}
}

uint8_t APISPI::waitForByte() {
	while (1) {
		uint32_t sreg = XSpi_GetStatusReg(&Spi);
		if (!(sreg & XSP_SR_RX_EMPTY_MASK)) {
			return XSpi_ReadReg(Spi.BaseAddr, XSP_DRR_OFFSET);
		}
	}
}

void APISPI::sendByte(uint8_t data) {
	while (XSpi_GetStatusReg(&Spi) & XSP_SR_TX_FULL_MASK) ;
	XSpi_WriteReg(Spi.BaseAddr, XSP_DTR_OFFSET, data);
}

uint32_t APISPI::readln(void* buffer, uint32_t bufferSize) {
	flushReceiveBuffer();
	GPIO::setBit(GPIO_IO9);	// signal, it wants to receive

	char* dptr = (char*) buffer;

	uint16_t len = waitForByte();
	len |= waitForByte() << 8;

	if (len >= bufferSize-1) {
		return 0;
	}

	for (int i=0;i<len;i++) {
		dptr[i] = waitForByte();
	}
	return len;
}

void APISPI::writeln(void* buffer, uint32_t bufferSize) {
	GPIO::clrBit(GPIO_IO9);	// signal, it wants to send

	char* sptr = (char*) buffer;

	sendByte(bufferSize & 0xff);
	sendByte(bufferSize >> 8);

	for (uint32_t i=0;i<bufferSize;i++) {
		sendByte(sptr[i]);
	}
}


