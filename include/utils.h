/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/
#ifndef __UTILS_H__
#define __UTILS_H__
#include "iota/iota_types.h"

#define BUNDLEHASH_LEN	81

bool validateTrytes(const char* trytes, int len, bool checkzero = true);
bool validateTrytesMaxLen(const char* trytes, int maxLen, bool checkzero = true);
bool validateHex(const char* hex, int len, bool checkzero = true);
bool hexToBytes(const char* key, uint8_t* bytes, int bytesLength);
int decrement_tryte(int max, tryte_t *tryte);
int increment_tryte(int max, tryte_t *tryte);
//void normalize_hash_fragment(tryte_t *fragment_trytes);
bool normalize_hash(tryte_t *hash_trytes, int secLevel);
bool validateTrytes(const char* trytes, int len, bool checkzero);
bool validateBase64(const char* base64, int len, bool checkzero = true);
bool asciiToTrytes(const char* src, char* dst, int bufferSize);
bool validateBIPPath(const char* p, int maxDepth, uint32_t* out);
bool validateString(const char* s);
#endif
