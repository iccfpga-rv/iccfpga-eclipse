#pragma once

#include <stdint.h>

#include "xparameters.h"
#include "xgpio.h"
#include "xdebug.h"
#include "xspi.h"		/* SPI device driver */

class APIInterface {
public:
	virtual ~APIInterface() {}
	virtual uint32_t readln(void* buffer, uint32_t bufferSize);
	virtual void writeln(void* buffer, uint32_t bufferSize);
	virtual bool init();
};

class APIUART : public APIInterface {
public:
	APIUART();
	virtual ~APIUART();

	virtual uint32_t readln(void* buffer, uint32_t bufferSize);
	virtual void writeln(void* buffer, uint32_t bufferSize);
	virtual bool init();
};

class APISPI : public APIInterface {
protected:
	void flushReceiveBuffer();
	uint8_t waitForByte();
	void sendByte(uint8_t data);

public:
	APISPI();
	virtual ~APISPI();

	virtual uint32_t readln(void* buffer, uint32_t bufferSize);
	virtual void writeln(void* buffer, uint32_t bufferSize);
	virtual bool init();

	XSpi* getSPI();
};
