################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/api/API.cpp \
../src/api/APIDefault.cpp \
../src/api/APIFlash.cpp \
../src/api/APIInterface.cpp \
../src/api/APIPoW.cpp \
../src/api/APISecureElement.cpp \
../src/api/APIWallet.cpp 

OBJS += \
./src/api/API.o \
./src/api/APIDefault.o \
./src/api/APIFlash.o \
./src/api/APIInterface.o \
./src/api/APIPoW.o \
./src/api/APISecureElement.o \
./src/api/APIWallet.o 

CPP_DEPS += \
./src/api/API.d \
./src/api/APIDefault.d \
./src/api/APIFlash.d \
./src/api/APIInterface.d \
./src/api/APIPoW.d \
./src/api/APISecureElement.d \
./src/api/APIWallet.d 


# Each subdirectory must supply rules for building sources it contributes
src/api/%.o: ../src/api/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C++ Compiler'
	riscv32-unknown-elf-g++ -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DOS_USE_CPP_INTERRUPTS -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DARDUINOJSON_EMBEDDED_MODE=1 -DARDUINOJSON_USE_LONG_LONG=1 -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu++14 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


