################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../submodule/trezor-crypto/tools/bip39bruteforce.c \
../submodule/trezor-crypto/tools/mktable.c \
../submodule/trezor-crypto/tools/xpubaddrgen.c 

OBJS += \
./submodule/trezor-crypto/tools/bip39bruteforce.o \
./submodule/trezor-crypto/tools/mktable.o \
./submodule/trezor-crypto/tools/xpubaddrgen.o 

C_DEPS += \
./submodule/trezor-crypto/tools/bip39bruteforce.d \
./submodule/trezor-crypto/tools/mktable.d \
./submodule/trezor-crypto/tools/xpubaddrgen.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/trezor-crypto/tools/%.o: ../submodule/trezor-crypto/tools/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32ima -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


