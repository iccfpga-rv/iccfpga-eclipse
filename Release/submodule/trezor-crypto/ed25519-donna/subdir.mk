################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../submodule/trezor-crypto/ed25519-donna/curve25519-donna-32bit.c \
../submodule/trezor-crypto/ed25519-donna/curve25519-donna-helpers.c \
../submodule/trezor-crypto/ed25519-donna/curve25519-donna-scalarmult-base.c \
../submodule/trezor-crypto/ed25519-donna/ed25519-donna-32bit-tables.c \
../submodule/trezor-crypto/ed25519-donna/ed25519-donna-basepoint-table.c \
../submodule/trezor-crypto/ed25519-donna/ed25519-donna-impl-base.c \
../submodule/trezor-crypto/ed25519-donna/ed25519-keccak.c \
../submodule/trezor-crypto/ed25519-donna/ed25519-sha3.c \
../submodule/trezor-crypto/ed25519-donna/ed25519.c \
../submodule/trezor-crypto/ed25519-donna/modm-donna-32bit.c 

OBJS += \
./submodule/trezor-crypto/ed25519-donna/curve25519-donna-32bit.o \
./submodule/trezor-crypto/ed25519-donna/curve25519-donna-helpers.o \
./submodule/trezor-crypto/ed25519-donna/curve25519-donna-scalarmult-base.o \
./submodule/trezor-crypto/ed25519-donna/ed25519-donna-32bit-tables.o \
./submodule/trezor-crypto/ed25519-donna/ed25519-donna-basepoint-table.o \
./submodule/trezor-crypto/ed25519-donna/ed25519-donna-impl-base.o \
./submodule/trezor-crypto/ed25519-donna/ed25519-keccak.o \
./submodule/trezor-crypto/ed25519-donna/ed25519-sha3.o \
./submodule/trezor-crypto/ed25519-donna/ed25519.o \
./submodule/trezor-crypto/ed25519-donna/modm-donna-32bit.o 

C_DEPS += \
./submodule/trezor-crypto/ed25519-donna/curve25519-donna-32bit.d \
./submodule/trezor-crypto/ed25519-donna/curve25519-donna-helpers.d \
./submodule/trezor-crypto/ed25519-donna/curve25519-donna-scalarmult-base.d \
./submodule/trezor-crypto/ed25519-donna/ed25519-donna-32bit-tables.d \
./submodule/trezor-crypto/ed25519-donna/ed25519-donna-basepoint-table.d \
./submodule/trezor-crypto/ed25519-donna/ed25519-donna-impl-base.d \
./submodule/trezor-crypto/ed25519-donna/ed25519-keccak.d \
./submodule/trezor-crypto/ed25519-donna/ed25519-sha3.d \
./submodule/trezor-crypto/ed25519-donna/ed25519.d \
./submodule/trezor-crypto/ed25519-donna/modm-donna-32bit.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/trezor-crypto/ed25519-donna/%.o: ../submodule/trezor-crypto/ed25519-donna/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32ima -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


