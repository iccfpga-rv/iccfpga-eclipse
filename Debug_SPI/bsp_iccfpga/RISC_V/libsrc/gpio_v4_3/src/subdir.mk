################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio.c \
../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_extra.c \
../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_g.c \
../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_intr.c \
../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_selftest.c \
../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_sinit.c 

OBJS += \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio.o \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_extra.o \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_g.o \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_intr.o \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_selftest.o \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_sinit.o 

C_DEPS += \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio.d \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_extra.d \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_g.d \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_intr.d \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_selftest.d \
./bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/xgpio_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/%.o: ../bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DOS_USE_CPP_INTERRUPTS -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DARDUINOJSON_EMBEDDED_MODE=1 -DARDUINOJSON_USE_LONG_LONG=1 -DUSE_SPI_SLAVE_INTERFACE -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


