################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../submodule/iccfpga-iota-lib/src/iota/addresses.cpp \
../submodule/iccfpga-iota-lib/src/iota/bundle.cpp \
../submodule/iccfpga-iota-lib/src/iota/conversion.cpp \
../submodule/iccfpga-iota-lib/src/iota/kerl.cpp \
../submodule/iccfpga-iota-lib/src/iota/seed.cpp \
../submodule/iccfpga-iota-lib/src/iota/signing.cpp \
../submodule/iccfpga-iota-lib/src/iota/transaction.cpp \
../submodule/iccfpga-iota-lib/src/iota/transfers.cpp 

OBJS += \
./submodule/iccfpga-iota-lib/src/iota/addresses.o \
./submodule/iccfpga-iota-lib/src/iota/bundle.o \
./submodule/iccfpga-iota-lib/src/iota/conversion.o \
./submodule/iccfpga-iota-lib/src/iota/kerl.o \
./submodule/iccfpga-iota-lib/src/iota/seed.o \
./submodule/iccfpga-iota-lib/src/iota/signing.o \
./submodule/iccfpga-iota-lib/src/iota/transaction.o \
./submodule/iccfpga-iota-lib/src/iota/transfers.o 

CPP_DEPS += \
./submodule/iccfpga-iota-lib/src/iota/addresses.d \
./submodule/iccfpga-iota-lib/src/iota/bundle.d \
./submodule/iccfpga-iota-lib/src/iota/conversion.d \
./submodule/iccfpga-iota-lib/src/iota/kerl.d \
./submodule/iccfpga-iota-lib/src/iota/seed.d \
./submodule/iccfpga-iota-lib/src/iota/signing.d \
./submodule/iccfpga-iota-lib/src/iota/transaction.d \
./submodule/iccfpga-iota-lib/src/iota/transfers.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/iccfpga-iota-lib/src/iota/%.o: ../submodule/iccfpga-iota-lib/src/iota/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C++ Compiler'
	riscv32-unknown-elf-g++ -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DARDUINOJSON_EMBEDDED_MODE=1 -DENABLE_FPGA_TESTS -DARDUINOJSON_USE_LONG_LONG=1 -DUSE_SPI_SLAVE_INTERFACE -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu++14 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


