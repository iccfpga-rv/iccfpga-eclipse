################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../submodule/trezor-crypto/bignum.c \
../submodule/trezor-crypto/bip32.c \
../submodule/trezor-crypto/bip39.c \
../submodule/trezor-crypto/curves.c \
../submodule/trezor-crypto/hmac.c \
../submodule/trezor-crypto/memzero.c \
../submodule/trezor-crypto/pbkdf2.c \
../submodule/trezor-crypto/secp256k1.c \
../submodule/trezor-crypto/sha2.c \
../submodule/trezor-crypto/slip39.c 

OBJS += \
./submodule/trezor-crypto/bignum.o \
./submodule/trezor-crypto/bip32.o \
./submodule/trezor-crypto/bip39.o \
./submodule/trezor-crypto/curves.o \
./submodule/trezor-crypto/hmac.o \
./submodule/trezor-crypto/memzero.o \
./submodule/trezor-crypto/pbkdf2.o \
./submodule/trezor-crypto/secp256k1.o \
./submodule/trezor-crypto/sha2.o \
./submodule/trezor-crypto/slip39.o 

C_DEPS += \
./submodule/trezor-crypto/bignum.d \
./submodule/trezor-crypto/bip32.d \
./submodule/trezor-crypto/bip39.d \
./submodule/trezor-crypto/curves.d \
./submodule/trezor-crypto/hmac.d \
./submodule/trezor-crypto/memzero.d \
./submodule/trezor-crypto/pbkdf2.d \
./submodule/trezor-crypto/secp256k1.d \
./submodule/trezor-crypto/sha2.d \
./submodule/trezor-crypto/slip39.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/trezor-crypto/%.o: ../submodule/trezor-crypto/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DARDUINOJSON_EMBEDDED_MODE=1 -DENABLE_FPGA_TESTS -DARDUINOJSON_USE_LONG_LONG=1 -DUSE_SPI_SLAVE_INTERFACE -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


