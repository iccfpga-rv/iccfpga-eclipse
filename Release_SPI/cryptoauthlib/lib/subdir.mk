################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cryptoauthlib/lib/atca_cfgs.c \
../cryptoauthlib/lib/atca_command.c \
../cryptoauthlib/lib/atca_device.c \
../cryptoauthlib/lib/atca_execution.c \
../cryptoauthlib/lib/atca_iface.c 

OBJS += \
./cryptoauthlib/lib/atca_cfgs.o \
./cryptoauthlib/lib/atca_command.o \
./cryptoauthlib/lib/atca_device.o \
./cryptoauthlib/lib/atca_execution.o \
./cryptoauthlib/lib/atca_iface.o 

C_DEPS += \
./cryptoauthlib/lib/atca_cfgs.d \
./cryptoauthlib/lib/atca_command.d \
./cryptoauthlib/lib/atca_device.d \
./cryptoauthlib/lib/atca_execution.d \
./cryptoauthlib/lib/atca_iface.d 


# Each subdirectory must supply rules for building sources it contributes
cryptoauthlib/lib/%.o: ../cryptoauthlib/lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DARDUINOJSON_EMBEDDED_MODE=1 -DENABLE_FPGA_TESTS -DARDUINOJSON_USE_LONG_LONG=1 -DUSE_SPI_SLAVE_INTERFACE -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


